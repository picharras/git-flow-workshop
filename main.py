from rockstars import Josue
from rockstars import TrueHome
from rockstars import JoseRipoll


def main():
    Josue.greet()
    TrueHome.greet()
    JoseRipoll.greet()
    print('Hola mundo a todos')

if __name__ == "__main__":
    main()
